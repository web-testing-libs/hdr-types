#include "hdr-shared-mutex.h++"



namespace icL { namespace hdr {

#ifdef HDR_USE_STD_SHARED_MUTEX

void SharedMutex::lock() {
    mutex.lock();
}

void SharedMutex::unlock() {
    mutex.unlock();
}

void SharedMutex::lock_shared() {
    mutex.lock_shared();
}

void SharedMutex::unlock_shared() {
    mutex.unlock_shared();
}

#else

void SharedMutex::lock() {
    while (counter > 0)
        ;
    mutex.lock();
}

void SharedMutex::unlock() {
    mutex.unlock();
}

void SharedMutex::lock_shared() {
    mutex.lock();
    ++counter;
    mutex.unlock();
}

void SharedMutex::unlock_shared() {
    assert(counter != 0);
    mutex.lock();
    --counter;
    mutex.unlock();
}

#endif

}}  // namespace icL::hdr
