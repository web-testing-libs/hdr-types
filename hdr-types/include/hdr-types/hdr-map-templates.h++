#ifndef icL_hdr_MapTemplates
#define icL_hdr_MapTemplates

#include "hdr-engined-map.h++"
#include "hdr-map-core.h++"
#include "hdr-map-engine.h++"

namespace icL { namespace hdr {

/**
 * class MicroFrontendCore use blocks of 4 items and 2 bit in each radix cicle
 */
template <
  typename Key, typename Value, typename Counter, typename Map, Counter max,
  typename Index>
using MicroFrontend =
  FrontendOfMap<Key, Value, Counter, Map, max, int8_t, 2, 0x3, Index>;

/**
 * class CommonFrontendCore use blocks of 16 items and 4 bit in each radix cicle
 */
template <
  typename Key, typename Value, typename Counter, typename Map, Counter max,
  typename Index>
using CommonFrontend =
  FrontendOfMap<Key, Value, Counter, Map, max, int8_t, 4, 0xF, Index>;

/**
 * class FastFrontendCore use blocks of 256 items and 8 bit in each radix cicle
 */
template <
  typename Key, typename Value, typename Counter, typename Map, Counter max,
  typename Index>
using FastFrontend =
  FrontendOfMap<Key, Value, Counter, Map, max, uint8_t, 8, 0xFF, Index>;

/**
 * class SuperFastFrontendCore use blocks of 65536 items and 16 bit in each
 * radix cicle
 */
template <
  typename Key, typename Value, typename Counter, typename Map, Counter max,
  typename Index>
using SuperFastFrontend =
  FrontendOfMap<Key, Value, Counter, Map, max, uint16_t, 16, 0xFFFF, Index>;

}}  // namespace icL::hdr

#endif  // icL_hdr_MapTemplates
