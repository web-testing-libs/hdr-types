#ifndef icL_hdr_MapEngine
#define icL_hdr_MapEngine

#include <cinttypes>



namespace icL { namespace hdr {

/**
 * @brief The EngineCore class contians commom parts of map engines
 */
template <typename Map>
class EngineCore
{
public:
    using MapT = Map;

    /**
     * @brief pointerGet get a pointer to a value
     * @param map is the hdr map
     * @param address is the address of needed value
     * @return get a pointer to a valid value
     */
    typename Map::ValueT * pointerGet(
      typename Map::MapT map, typename Map::AddressT * address) noexcept {
        return pointerGet_(map, address);
    }

    /**
     * @brief pointerGet get a pointer to a value
     * @param map is the hdr map
     * @param address is the address of needed value
     * @return get a pointer to a valid value
     */
    const typename Map::ValueT * pointerGet(
      typename Map::MapT map, typename Map::AddressT * address) const noexcept {
        return pointerGet_(map, address);
    }

    /**
     * @brief defaultGet get a value
     * @param map is the hdr map
     * @param address is the address of needed value
     * @param _default is the default value
     * @return a valid value
     */
    virtual const typename Map::ValueT defaultGet(
      typename Map::MapT map, typename Map::AddressT * address,
      const typename Map::ValueT & _default) const noexcept = 0;

protected:
    virtual typename Map::ValueT * pointerGet_(
      typename Map::MapT map, typename Map::AddressT * address) const
      noexcept = 0;

    template <typename Type>
    static Type * initedArray() {
        Type * ret = new Type[Map::arraySizeV];

        for (int64_t i = 0; i < Map::arraySizeV; i++) {
            ret[i] = nullptr;
        }

        return ret;
    }

    template <typename T>
    static void ensureMap(typename Map::MapT & map) {
        if (map == nullptr) {
            map = initedArray<T>();
        }
    }

    template <typename T>
    static T * ensureLevel(T ** container, typename Map::AddressT * address) {
        T *& var = container[*address];

        if (var == nullptr) {
            var = initedArray<T>();
        }

        return var;
    }

    template <typename T>
    static void finishLevel(T * container, typename Map::AddressT * address) {
        typename Map::ValueT *& var = container[*address];

        if (var == nullptr)
            var = new typename Map::ValueT[Map::arraySizeV];
    }

    template <typename T>
    static T * getLevel(T ** container, typename Map::AddressT * address) {
        return container == nullptr ? nullptr : container[*address];
    }
};

#define ecore EngineCore<Map>::template

/**
 * @brief The Engine4D class is a realition of HDR Map algorith with a 4d matrix
 */
template <typename Map>
class Engine4D : public EngineCore<Map>
{
public:
    /**
     * @brief ensureAddressCell unsures that address cell exists and is valid
     */
    void ensureAddressCell(
      typename Map::MapT & map, typename Map::AddressT * address) noexcept {
        using Value = typename Map::ValueT;

        ecore ensureMap<Value ***>(map);

        auto lev1 = ecore ensureLevel<Value **>(map, address + 0);
        auto lev2 = ecore ensureLevel<Value *>(lev1, address + 1);

        ecore finishLevel(lev2, address + 2);
    }

    const typename Map::ValueT defaultGet(
      typename Map::MapT map, typename Map::AddressT * address,
      const typename Map::ValueT & _default) const noexcept override {
        using Value = typename Map::ValueT;

        auto lev1 = ecore getLevel<Value **>(map, address + 0);
        auto lev2 = ecore getLevel<Value *>(lev1, address + 1);
        auto lev3 = ecore getLevel<Value>(lev2, address + 2);

        return lev3 == nullptr ? _default : lev3[address[3]];
    }

protected:
    typename Map::ValueT * pointerGet_(
      typename Map::MapT map, typename Map::AddressT * address) const
      noexcept override {
        return &map[address[0]][address[1]][address[2]][address[3]];
    }
};

/**
 * @brief The Engine8D class is a realition of HDR Map algorith with a 8d matrix
 */
template <typename Map>
class Engine8D : public EngineCore<Map>
{
public:
    /**
     * @brief ensureAddressCell unsures that address cell exists and is valid
     */
    void ensureAddressCell(
      typename Map::MapT & map, typename Map::AddressT * address) noexcept {
        using Value = typename Map::ValueT;

        ecore ensureMap<Value *******>(map);

        auto lev1 = ecore ensureLevel<Value ******>(map, address + 0);
        auto lev2 = ecore ensureLevel<Value *****>(lev1, address + 1);
        auto lev3 = ecore ensureLevel<Value ****>(lev2, address + 2);
        auto lev4 = ecore ensureLevel<Value ***>(lev3, address + 3);
        auto lev5 = ecore ensureLevel<Value **>(lev4, address + 4);
        auto lev6 = ecore ensureLevel<Value *>(lev5, address + 5);

        ecore finishLevel(lev6, address + 6);
    }

    const typename Map::ValueT defaultGet(
      typename Map::MapT map, typename Map::AddressT * address,
      const typename Map::ValueT & _default) const noexcept override {
        using Value = typename Map::ValueT;

        auto lev1 = ecore getLevel<Value ******>(map, address + 0);
        auto lev2 = ecore getLevel<Value *****>(lev1, address + 1);
        auto lev3 = ecore getLevel<Value ****>(lev2, address + 2);
        auto lev4 = ecore getLevel<Value ***>(lev3, address + 3);
        auto lev5 = ecore getLevel<Value **>(lev4, address + 4);
        auto lev6 = ecore getLevel<Value *>(lev5, address + 5);
        auto lev7 = ecore getLevel<Value>(lev6, address + 6);

        return lev7 == nullptr ? _default : lev7[address[7]];
    }

protected:
    typename Map::ValueT * pointerGet_(
      typename Map::MapT map, typename Map::AddressT * address) const
      noexcept override {
        return &map[address[0]][address[1]][address[2]][address[3]][address[4]]
                   [address[5]][address[6]][address[7]];
    }
};

/**
 * @brief The Engine16D class is a realition of HDR Map algorith with a 16d
 * matrix
 */
template <typename Map>
class Engine16D : public EngineCore<Map>
{
public:
    /**
     * @brief ensureAddressCell unsures that address cell exists and is valid
     */
    void ensureAddressCell(
      typename Map::MapT & map, typename Map::AddressT * address) noexcept {
        ecore ensureMap<typename Map::ValueT ***************>(map);
        using Value = typename Map::ValueT;

        ecore ensureMap<Value ***************>(map);

        auto lev1  = ecore ensureLevel<Value **************>(map, address + 0);
        auto lev2  = ecore ensureLevel<Value *************>(lev1, address + 1);
        auto lev3  = ecore ensureLevel<Value ************>(lev2, address + 2);
        auto lev4  = ecore ensureLevel<Value ***********>(lev3, address + 3);
        auto lev5  = ecore ensureLevel<Value **********>(lev4, address + 4);
        auto lev6  = ecore ensureLevel<Value *********>(lev5, address + 5);
        auto lev7  = ecore ensureLevel<Value ********>(lev6, address + 6);
        auto lev8  = ecore ensureLevel<Value *******>(lev7, address + 7);
        auto lev9  = ecore ensureLevel<Value ******>(lev8, address + 8);
        auto lev10 = ecore ensureLevel<Value *****>(lev9, address + 9);
        auto lev11 = ecore ensureLevel<Value ****>(lev10, address + 10);
        auto lev12 = ecore ensureLevel<Value ***>(lev11, address + 11);
        auto lev13 = ecore ensureLevel<Value **>(lev12, address + 12);
        auto lev14 = ecore ensureLevel<Value *>(lev13, address + 13);

        ecore finishLevel(lev14, address + 14);
    }

    const typename Map::ValueT defaultGet(
      typename Map::MapT map, typename Map::AddressT * address,
      const typename Map::ValueT & _default) const noexcept override {
        using Value = typename Map::ValueT;

        auto lev1  = ecore getLevel<Value **************>(map, address + 0);
        auto lev2  = ecore getLevel<Value *************>(lev1, address + 1);
        auto lev3  = ecore getLevel<Value ************>(lev2, address + 2);
        auto lev4  = ecore getLevel<Value ***********>(lev3, address + 3);
        auto lev5  = ecore getLevel<Value **********>(lev4, address + 4);
        auto lev6  = ecore getLevel<Value *********>(lev5, address + 5);
        auto lev7  = ecore getLevel<Value ********>(lev6, address + 6);
        auto lev8  = ecore getLevel<Value *******>(lev7, address + 7);
        auto lev9  = ecore getLevel<Value ******>(lev8, address + 8);
        auto lev10 = ecore getLevel<Value *****>(lev9, address + 9);
        auto lev11 = ecore getLevel<Value ****>(lev10, address + 10);
        auto lev12 = ecore getLevel<Value ***>(lev11, address + 11);
        auto lev13 = ecore getLevel<Value **>(lev12, address + 12);
        auto lev14 = ecore getLevel<Value *>(lev13, address + 13);
        auto lev15 = ecore getLevel<Value>(lev14, address + 14);

        return lev15 == nullptr ? _default : lev15[address[15]];
    }

protected:
    typename Map::ValueT * pointerGet_(
      typename Map::MapT map, typename Map::AddressT * address) const
      noexcept override {
        return &map[address[0]][address[1]][address[2]][address[3]][address[4]]
                   [address[5]][address[6]][address[7]][address[8]][address[9]]
                   [address[10]][address[11]][address[12]][address[13]]
                   [address[14]][address[15]];
    }
};

}}  // namespace icL::hdr

#endif
