#ifndef icL_hdr_EnginedMap
#define icL_hdr_EnginedMap

#include <inttypes.h>



namespace icL { namespace hdr {

/**
 * @brief The EnginedMap class combine a map and an engine
 */
template <typename Engine>
class EnginedMap : public Engine::MapT
{
    /// @brief engine is used to manipulate data in map
    Engine engine;

public:
    using KeyT   = typename Engine::MapT::KeyT;
    using ValueT = typename Engine::MapT::ValueT;

    template <
      typename, typename, typename Counter, typename, Counter, typename, int,
      int, uint32_t, typename>
    friend class BackendOfMap;

protected:
    void ensureAddressCell() noexcept override {
        engine.ensureAddressCell(this->getMap(), this->getAddress());
    }

    [[nodiscard]] typename Engine::MapT::ValueT * pointerGet(
      const typename Engine::MapT::KeyT & key) noexcept override {
        this->calcAddressFor(key);
        return engine.pointerGet(this->getMap(), this->getAddress());
    }

    [[nodiscard]] const typename Engine::MapT::ValueT * pointerGet(
      const typename Engine::MapT::KeyT & key) const noexcept override {
        this->calcAddressFor(key);
        return engine.pointerGet(this->getMap(), this->getAddress());
    }

    [[nodiscard]] typename Engine::MapT::ValueT defaultGet(
      const typename Engine::MapT::KeyT & key) const noexcept override {
        static const typename Engine::MapT::ValueT _default = {};

        this->calcAddressFor(key);
        return engine.defaultGet(this->getMap(), this->getAddress(), _default);
    }
};

}}  // namespace icL::hdr

#endif  // icL_hdr_EnginedMap
