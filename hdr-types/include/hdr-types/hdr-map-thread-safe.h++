#ifndef icL_hdr_MapThreadSafe
#define icL_hdr_MapThreadSafe

#include "hdr-shared-mutex.h++"



namespace icL { namespace hdr {

/**
 * @brief The MapThreadSafe class makes a map thread safe
 */
template <typename Map>
class ThreadSafeMap : public Map
{
    /// @brief mutex makes this object thread safe
    SharedMutex mutex;

public:
    using KeyT   = typename Map::KeyT;
    using ValueT = typename Map::ValueT;

public:
    bool empty() noexcept {
        mutex.lock_shared();
        bool ret = Map::empty();
        mutex.unlock_shared();
        return ret;
    }

    typename Map::CounterT size() noexcept {
        mutex.lock_shared();
        typename Map::CounterT ret = Map::size();
        mutex.unlock_shared();
        return ret;
    }

    typename Map::CounterT maxSize() noexcept {
        mutex.lock_shared();
        typename Map::CounterT ret = Map::maxSize();
        mutex.unlock_shared();
        return ret;
    }

    typename Map::ValueT & at(
      const typename Map::KeyT & key) noexcept override {
        mutex.lock_shared();
        typename Map::ValueT & ret = Map::at(key);
        mutex.unlock_shared();
        return ret;
    }

    void insert(
      const typename Map::KeyT &   key,
      const typename Map::ValueT & value) override {
        mutex.lock_shared();
        Map::insert(key, value);
        mutex.unlock_shared();
    }

    void erase(const typename Map::KeyT & key) noexcept override {
        mutex.lock_shared();
        Map::erase(key);
        mutex.unlock_shared();
    }
};

}}  // namespace icL::hdr

#endif  // icL_hdr_MapThreadSafe
