#ifndef icL_hdr_IMap
#define icL_hdr_IMap

#include <cinttypes>



namespace icL { namespace hdr {

/**
 * @brief The IMap struct is a interface to a map
 */
template <typename Key, typename Value, typename Counter>
struct IMap
{
    // properties read
    [[nodiscard]] virtual bool    empty() const noexcept   = 0;
    [[nodiscard]] virtual Counter size() const noexcept    = 0;
    [[nodiscard]] virtual Counter maxSize() const noexcept = 0;

    // element access
    [[nodiscard]] virtual const Value & at(const Key & key) const noexcept = 0;
    [[nodiscard]] virtual Value &       at(const Key & key) noexcept       = 0;

    // modifiers
    virtual void insert(const Key & key, const Value & value) = 0;
    virtual void erase(const Key & key) noexcept              = 0;
};

template <typename Value>
using IInt8Map = IMap<int8_t, Value, int32_t>;

template <typename Value>
using IUInt8Map = IMap<uint8_t, Value, int32_t>;

template <typename Value>
using IInt16Map = IMap<int16_t, Value, int32_t>;

template <typename Value>
using IUInt16Map = IMap<uint16_t, Value, int32_t>;

template <typename Value>
using IInt32Map = IMap<int32_t, Value, uint32_t>;

template <typename Value>
using IUInt32Map = IMap<uint32_t, Value, uint32_t>;

template <typename Value>
using IInt64Map = IMap<int64_t, Value, uint64_t>;

template <typename Value>
using IUInt64Map = IMap<uint64_t, Value, uint64_t>;

#if defined(__SIZEOF_INT128__)

#define HDR_128BIT_SUPPORTED

template <typename Value>
using IInt128Map = IMap<__int128_t, Value, __uint128_t>;

template <typename Value>
using IUInt128Map = IMap<__uint128_t, Value, __uint128_t>;

#endif

}}  // namespace icL::hdr

#endif  // icL_hdr_IMap
