#ifndef icL_hdr_Map
#define icL_hdr_Map

#include "hdr-engined-map.h++"
#include "hdr-map-engine.h++"
#include "hdr-map-templates.h++"
#include "hdr-map-thread-safe.h++"

#include <cinttypes>



namespace icL { namespace hdr {

// 8 by 2

template <typename Key, typename Value, typename Index>
using KeyMap8MicroCore = EnginedMap<
  Engine4D<MicroFrontend<Key, Value, int32_t, Value ****, 0xFF, Index>>>;

template <typename Key, typename Value>
using KeyMap8Micro =
  KeyMap8MicroCore<Key, Value, KeyMap8MicroCore<Key, uint8_t, MockOfMap<Key>>>;

template <typename Key, typename Value>
using KeyMap8MicroTS = ThreadSafeMap<KeyMap8Micro<Key, Value>>;

template <typename Value>
using Int8MicroMap = KeyMap8Micro<int8_t, Value>;

template <typename Value>
using Int8MicroThreadSafeMap = KeyMap8MicroTS<int8_t, Value>;

template <typename Value>
using UInt8MicroMap = KeyMap8Micro<uint8_t, Value>;

template <typename Value>
using UInt8MicroThreadSafeMap = KeyMap8MicroTS<uint8_t, Value>;

// 16 by 2

template <typename Key, typename Value, typename Index>
using KeyMap16MicroCore = EnginedMap<
  Engine8D<MicroFrontend<Key, Value, int32_t, Value ********, 0xFFFF, Index>>>;

template <typename Key, typename Value>
using KeyMap16Micro = KeyMap16MicroCore<
  Key, Value, KeyMap16MicroCore<Key, uint8_t, MockOfMap<Key>>>;

template <typename Key, typename Value>
using KeyMap16MicroTS = ThreadSafeMap<KeyMap16Micro<Key, Value>>;

template <typename Value>
using Int16MicroMap = KeyMap16Micro<int16_t, Value>;

template <typename Value>
using Int16MicroThreadSafeMap = KeyMap16MicroTS<int16_t, Value>;

template <typename Value>
using UInt16MicroMap = KeyMap16Micro<uint16_t, Value>;

template <typename Value>
using UInt16MicroThreadSafeMap = KeyMap16MicroTS<uint16_t, Value>;

// 16 by 4

template <typename Key, typename Value, typename Index>
using KeyMap16CommonCore = EnginedMap<
  Engine4D<CommonFrontend<Key, Value, int32_t, Value ****, 0xFFFF, Index>>>;

template <typename Key, typename Value>
using KeyMap16Common = KeyMap16CommonCore<
  Key, Value, KeyMap16CommonCore<Key, uint8_t, MockOfMap<Key>>>;

template <typename Key, typename Value>
using KeyMap16CommonTS = ThreadSafeMap<KeyMap16Common<Key, Value>>;

template <typename Value>
using Int16Map = KeyMap16Common<int16_t, Value>;

template <typename Value>
using Int16ThreadSafeMap = KeyMap16CommonTS<int16_t, Value>;

template <typename Value>
using UInt16Map = KeyMap16Common<uint16_t, Value>;

template <typename Value>
using UInt16ThreadSafeMap = KeyMap16CommonTS<uint16_t, Value>;

// 32 by 2

template <typename Key, typename Value, typename Index>
using KeyMap32MicroCore = EnginedMap<Engine16D<MicroFrontend<
  Key, Value, uint32_t, Value ****************, 0xFFFFFFFF, Index>>>;

template <typename Key, typename Value>
using KeyMap32Micro = KeyMap32MicroCore<
  Key, Value, KeyMap32MicroCore<Key, uint8_t, MockOfMap<Key>>>;

template <typename Key, typename Value>
using KeyMap32MicroTS = ThreadSafeMap<KeyMap32Micro<Key, Value>>;

template <typename Value>
using Int32MicroMap = KeyMap32Micro<int32_t, Value>;

template <typename Value>
using Int32MicroThreadSafeMap = KeyMap32MicroTS<int32_t, Value>;

template <typename Value>
using UInt32MicroMap = KeyMap32Micro<uint32_t, Value>;

template <typename Value>
using UInt32MicroThreadSafeMap = KeyMap32MicroTS<uint32_t, Value>;

// 32 by 4

template <typename Key, typename Value, typename Index>
using KeyMap32CommonCore = EnginedMap<Engine8D<
  CommonFrontend<Key, Value, uint32_t, Value ********, 0xFFFFFFFF, Index>>>;

template <typename Key, typename Value>
using KeyMap32Common = KeyMap32CommonCore<
  Key, Value, KeyMap32CommonCore<Key, uint8_t, MockOfMap<Key>>>;

template <typename Key, typename Value>
using KeyMap32CommonTS = ThreadSafeMap<KeyMap32Common<Key, Value>>;

template <typename Value>
using Int32Map = KeyMap32Common<int32_t, Value>;

template <typename Value>
using Int32ThreadSafeMap = KeyMap32CommonTS<int32_t, Value>;

template <typename Value>
using UInt32Map = KeyMap32Common<uint32_t, Value>;

template <typename Value>
using UInt32ThreadSafeMap = KeyMap32CommonTS<uint32_t, Value>;

// 32 by 8

template <typename Key, typename Value, typename Index>
using KeyMap32FastCore = EnginedMap<
  Engine4D<FastFrontend<Key, Value, uint32_t, Value ****, 0xFFFFFFFF, Index>>>;

template <typename Key, typename Value>
using KeyMap32Fast =
  KeyMap32FastCore<Key, Value, KeyMap32FastCore<Key, uint8_t, MockOfMap<Key>>>;

template <typename Key, typename Value>
using KeyMap32FastTS = ThreadSafeMap<KeyMap32Fast<Key, Value>>;

template <typename Value>
using Int32FastMap = KeyMap32Fast<int32_t, Value>;

template <typename Value>
using Int32FastThreadSafeMap = KeyMap32FastTS<int32_t, Value>;

template <typename Value>
using UInt32FastMap = KeyMap32Fast<uint32_t, Value>;

template <typename Value>
using UInt32FastThreadSafeMap = KeyMap32FastTS<uint32_t, Value>;

// 64 by 4

template <typename Key, typename Value, typename Index>
using KeyMap64CommonCore = EnginedMap<Engine16D<CommonFrontend<
  Key, Value, uint64_t, Value ****************, 0xFFFFFFFFFFFFFFFF, Index>>>;

template <typename Key, typename Value>
using KeyMap64Common = KeyMap64CommonCore<
  Key, Value, KeyMap64CommonCore<Key, uint8_t, MockOfMap<Key>>>;

template <typename Key, typename Value>
using KeyMap64CommonTS = ThreadSafeMap<KeyMap64Common<Key, Value>>;

template <typename Value>
using Int64Map = KeyMap64Common<int64_t, Value>;

template <typename Value>
using Int64ThreadSafeMap = KeyMap64CommonTS<int64_t, Value>;

template <typename Value>
using UInt64Map = KeyMap64Common<uint64_t, Value>;

template <typename Value>
using UInt64ThreadSafeMap = KeyMap64CommonTS<uint64_t, Value>;

// 64 by 8

template <typename Key, typename Value, typename Index>
using KeyMap64FastCore = EnginedMap<Engine8D<FastFrontend<
  Key, Value, uint64_t, Value ********, 0xFFFFFFFFFFFFFFFF, Index>>>;

template <typename Key, typename Value>
using KeyMap64Fast =
  KeyMap64FastCore<Key, Value, KeyMap64FastCore<Key, uint8_t, MockOfMap<Key>>>;

template <typename Key, typename Value>
using KeyMap64FastTS = ThreadSafeMap<KeyMap64Fast<Key, Value>>;

template <typename Value>
using Int64FastMap = KeyMap64Fast<int64_t, Value>;

template <typename Value>
using Int64FastThreadSafeMap = KeyMap64FastTS<int64_t, Value>;

template <typename Value>
using UInt64FastMap = KeyMap64Fast<uint64_t, Value>;

template <typename Value>
using UInt64FastThreadSafeMap = KeyMap64FastTS<uint64_t, Value>;

// 64 by 16

template <typename Key, typename Value, typename Index>
using KeyMap64SuperFastCore = EnginedMap<Engine4D<SuperFastFrontend<
  Key, Value, uint64_t, Value ****, 0xFFFFFFFFFFFFFFFF, Index>>>;

template <typename Key, typename Value>
using KeyMap64SuperFast = KeyMap64SuperFastCore<
  Key, Value, KeyMap64SuperFastCore<Key, uint8_t, MockOfMap<Key>>>;

template <typename Key, typename Value>
using KeyMap64SuperFastTS = ThreadSafeMap<KeyMap64SuperFast<Key, Value>>;

template <typename Value>
using Int64SuperFastMap = KeyMap64SuperFast<int64_t, Value>;

template <typename Value>
using Int64SuperFastThreadSafeMap = KeyMap64SuperFastTS<int64_t, Value>;

template <typename Value>
using UInt64SuperFastMap = KeyMap64SuperFast<uint64_t, Value>;

template <typename Value>
using UInt64SuperFastThreadSafeMap = KeyMap64SuperFastTS<uint64_t, Value>;

#if defined(__SIZEOF_INT128__)

// 128 by 8

const __uint128_t max128 = __uint128_t(0) - __uint128_t(1);

template <typename Key, typename Value, typename Index>
using KeyMap128FastCore = EnginedMap<Engine16D<FastFrontend<
  Key, Value, __uint128_t, Value ****************, max128, Index>>>;

template <typename Key, typename Value>
using KeyMap128Fast = KeyMap128FastCore<
  Key, Value, KeyMap128FastCore<Key, uint8_t, MockOfMap<Key>>>;

template <typename Key, typename Value>
using KeyMap128FastTS = ThreadSafeMap<KeyMap128Fast<Key, Value>>;

template <typename Value>
using Int128FastMap = KeyMap128Fast<__int128_t, Value>;

template <typename Value>
using Int128FastThreadSafeMap = KeyMap128FastTS<__int128_t, Value>;

template <typename Value>
using UInt128FastMap = KeyMap128Fast<__uint128_t, Value>;

template <typename Value>
using UInt128FastThreadSafeMap = KeyMap128FastTS<__uint128_t, Value>;

// 128 by 16

template <typename Key, typename Value, typename Index>
using KeyMap128SuperFastCore = EnginedMap<Engine8D<
  SuperFastFrontend<Key, Value, __uint128_t, Value ********, max128, Index>>>;

template <typename Key, typename Value>
using KeyMap128SuperFast = KeyMap128SuperFastCore<
  Key, Value, KeyMap128SuperFastCore<Key, uint8_t, MockOfMap<Key>>>;

template <typename Key, typename Value>
using KeyMap128SuperFastTS = KeyMap128SuperFast<Key, Value>;

template <typename Value>
using Int128SuperFastMap = KeyMap128SuperFast<__int128_t, Value>;

template <typename Value>
using Int128SuperFastThreadSafeMap = KeyMap128SuperFastTS<__int128_t, Value>;

template <typename Value>
using UInt128SuperFastMap = KeyMap128SuperFast<__uint128_t, Value>;

template <typename Value>
using UInt128SuperFastThreadSafeMap = KeyMap128SuperFastTS<__uint128_t, Value>;

#endif

}}  // namespace icL::hdr

#endif  // icL_hdr_Map
