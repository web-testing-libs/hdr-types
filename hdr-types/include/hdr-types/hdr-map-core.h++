#ifndef icL_hdr_MapCore
#define icL_hdr_MapCore

#include "hdr-imap.h++"

#include <cinttypes>
#include <initializer_list>
#include <utility>



namespace icL { namespace hdr {

enum Exceptions {
    FullMemory  ///< No more place to store data
};


/**
 * @brief The MockOfMap class simulates a map class
 */
template <typename Key>
class MockOfMap
{
public:
    /**
     * @brief defaultGet always return 0
     * @return 0 (always)
     */
    uint8_t defaultGet(const Key &) const noexcept {
        return 0;
    }

    /**
     * @brief referenceGet returns the same reference each time
     * @return a valid reference
     */
    uint8_t & referenceGet(const Key &) const noexcept {
        static uint8_t value;
        return value;
    }
};


/**
 * @brief The class MapBackend define a closed incomplete map realization
 *
 * The class MapPrivate has next template variables:
 * + Key - is the type of key
 * + Value - is the type of value
 * + Counter - is the type of value counter (commonly unsigned Key)
 * + Map - is the type to store map data (commonly Value****)
 * + Address - is the type to store path components ()
 * + addressSize - is the size of address array
 * + mask - is used to extract meaning bits from key
 * + Inited - is a type used to store inited flag
 * + enableInited - enables inited cache when is 1
 */
template <
  typename Key, typename Value, typename Counter, typename Map, Counter max,
  typename Address, int addressSize, int addressStep, uint32_t mask,
  typename InitedMap>
class BackendOfMap : public IMap<Key, Value, Counter>
{
private:
    /// @brief counter counts number of values in map
    Counter counter = 0x0;
    /// @brief counter_max is the maximum number of values in map
    Counter counter_max = max;
    /// @brief map is a pointer to map data
    Map map = nullptr;
    /// @brief inited contains the inited state of each item
    InitedMap inited;
    /// @brief address is a array of coordinates to currentKey
    Address * address = new Address[addressSize];
    /// @brief currentKey is the last key used to calc address
    Key currentKey = 0x0;

public:
    /**
     * @brief MapBackend is the default contructor
     * @param max is the max value of counter
     * @param map is the initial map value
     * @param address is the address value of map
     */
    BackendOfMap() = default;

    BackendOfMap(const BackendOfMap &)  = delete;
    BackendOfMap(const BackendOfMap &&) = delete;
    void operator=(const BackendOfMap &) = delete;
    void operator=(const BackendOfMap &&) = delete;

    /**
     * ~MapBackend deletes map data
     */
    ~BackendOfMap() {
        delete[] map;
        delete[] address;
    }

protected:
    /**
     * @brief getMap gets the pointer to map data
     * @return a pointer to map data
     */
    [[nodiscard]] Map getMap() const noexcept {
        return map;
    }

    /**
     * @brief getMap gets the pointer to map data
     * @return a pointer to map data
     */
    Map & getMap() noexcept {
        return map;
    }

    /**
     * @brief getInited gets the inited state of key value
     * @param key is the inited state of key
     * @return a bool value
     */
    [[nodiscard]] bool getInited(const Key & key) const noexcept {
        return inited.defaultGet(key >> 3) >> (key & 0x7) & 0x1;
    }

    /**
     * @brief setInited sets up the inited state of key value
     * @param key is the key of value
     */
    void setInited(const Key & key) noexcept {
        uint8_t & block = inited.referenceGet(key >> 3);
        uint8_t   bit   = 1 << (key & 0x7);
        block           = block | bit;
    }

    /**
     * @brief unsetInited unsets the inited state of key value
     * @param key
     */
    void unsetInited(const Key & key) noexcept {
        uint8_t & block = inited.referenceGet(key >> 3);
        uint8_t   bit   = 1 << (key & 0x7);
        block           = block & ~bit;
    }

    /**
     * @brief getAddres gets the address array
     * @return the address array
     */
    [[nodiscard]] Address * getAddress() const noexcept {
        return address;
    }

    /**
     * @brief calcAddressFor calculates adress for key and store it to address
     * @param key is the key to calculate address for
     */
    void calcAddressFor(Key key) const noexcept {
        for (int i = 0; i < addressSize; i++) {
            address[addressSize - i - 1] = key & mask;
            // Delete last addressStep bits
            key = key >> addressStep;
        }
    }

    /**
     * @brief calcAddressFor calculates adress for key and store it to address
     * @param key is the key to calculate address for
     */
    void calcAddressFor(Key key) noexcept {
        if (key == currentKey)
            return;

        static_cast<const BackendOfMap<
          Key, Value, Counter, Map, max, Address, addressSize, addressStep,
          mask, InitedMap> &>(*this)
          .calcAddressFor(key);

        currentKey = key;
    }

    /**
     * @brief ensureAddressCell unsures that address cell exists and is valid
     */
    virtual void ensureAddressCell() noexcept = 0;

    /**
     * @brief pointerGet get a pointer to a value
     * @param key is the key of needed value
     * @return get a reference to a valid value
     */
    [[nodiscard]] virtual Value * pointerGet(const Key & key) noexcept = 0;

    /**
     * @brief pointerGet get a pointer to a value
     * @param key is the key of needed value
     * @return get a reference to a valid value
     */
    [[nodiscard]] virtual const Value * pointerGet(const Key & key) const
      noexcept = 0;

    /**
     * @brief strongGet get a reference to a value (create it if not exists)
     * @param key is the key of needed value
     * @return get a reference to a valid value
     */
    virtual Value & referenceGet(const Key & key) noexcept {
        calcAddressFor(key);
        ensureAddressCell();
        return *pointerGet(key);
    }

    /**
     * @brief defaultGet gets a value or return a default value
     * @param key is the key of needed value
     */
    virtual Value defaultGet(const Key & key) const noexcept = 0;

public:
    // capacity

    /**
     * @brief empty checks if the map is empty
     * @return a bool value
     */
    [[nodiscard]] bool empty() const noexcept override {
        return counter == 0x0;
    }

    /**
     * @brief size returns the count of elements in map
     * @return the count of elements in map
     */
    [[nodiscard]] Counter size() const noexcept override {
        return counter;
    }

    /**
     * @brief maxSize returns the maximum size of map
     * @return the maximum size of map
     */
    [[nodiscard]] Counter maxSize() const noexcept override {
        return counter_max;
    }

    // element access

    /**
     * @brief at gets an element by key (it must exists)
     * @param key is the key of needed element
     * @return a value by key
     */
    [[nodiscard]] const Value & at(const Key & key) const noexcept override {
        return *pointerGet(key);
    }

    Value & at(const Key & key) noexcept override {
        return *pointerGet(key);
    }


    // modifiers

    /**
     * @brief insert inserts a value to map
     * @param key is the key of new value
     * @param value is the value itself
     */
    void insert(const Key & key, const Value & value) override {
        if (!getInited(key)) {
            counter++;
            setInited(key);
        }

        referenceGet(key) = value;
    }

    /**
     * @brief erase removes a value by key
     * @param key is the key of value
     */
    void erase(const Key & key) noexcept override {
        if (getInited(key)) {
            counter--;
            unsetInited(key);
        }
    }
};

/**
 * @brief The MapFrontend class defines a front end for a map backend
 */
template <
  typename Key, typename Value, typename Counter, typename Map, Counter max,
  typename Address, int addressStep, uint32_t mask, typename InitedMap>
class FrontendOfMap
    : public BackendOfMap<
        Key, Value, Counter, Map, max, Address, sizeof(Key) * 8 / addressStep,
        addressStep, mask, InitedMap>
{
public:
    using KeyT     = Key;
    using ValueT   = Value;
    using CounterT = Counter;
    using MapT     = Map;
    using AddressT = Address;

    static const int32_t addressSizeV = sizeof(Key) * 8 / addressStep;
    static const int32_t addressStepV = addressStep;
    static const int64_t arraySizeV   = int64_t(1) << int64_t(addressStep);

protected:
};

}}  // namespace icL::hdr

#endif  // icL_hdr_MapCore
