#ifndef icL_hdr_SharedMutex
#define icL_hdr_SharedMutex

#ifdef HDR_USE_STD_SHARED_MUTEX
#include <shared_mutex>
#else
#include <cassert>
#include <mutex>
#endif

namespace icL { namespace hdr {
#ifdef HDR_USE_STD_SHARED_MUTEX
class SharedMutex
{
    std::shared_mutex mutex;

public:
    void lock();
    void unlock();
    void lock_shared();
    void unlock_shared();
};
#else
/**
 * @brief The SharedMutex class is used to create thread safe classes
 *
 * The SharedMutex class is a synchronization primitive that can be used
 * to protect shared data from being simultaneously accessed by multiple
 * threads. In contrast to other mutex types which facilitate exclusive access,
 * a shared_mutex has two levels of access:
 * * shared - several threads can share ownership of the same mutex.
 * * exclusive - only one thread can own the mutex.
 * Shared mutexes are usually used in situations when multiple readers can
 * access the same resource at the same time without causing data races, but
 * only one writer can do so.
 */
class SharedMutex
{
    std::mutex   mutex;
    volatile int counter = 0;

public:
    /**
     * @brief lock locks the mutex, blocks if the mutex is not available
     */
    void lock();

    /**
     * @brief unlock unlocks the mutex
     */
    void unlock();

    /**
     * @brief lock_shared locks the mutex for shared ownership, blocks if the
     * mutex is not available
     */
    void lock_shared();

    /**
     * @brief unlock_shared unlocks the mutex (shared ownership)
     */
    void unlock_shared();
};
#endif

}}  // namespace icL::hdr

#endif  // icL_hdr_SharedMutex
