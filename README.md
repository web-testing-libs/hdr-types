## High Dynamical Range Associative Map

High Dynamical Range Associative Map (`hdr::Map`) is a data structure, which is similar to
`std::map` and `std::unordered_map`. HDR Map accepts as keys just integer values.

Let's compare!

| Algorithm                                  | Look up  |           | Insertion |           | Deletion |           | Ordered |
|--------------------------------------------|----------|-----------|-----------|-----------|----------|-----------|---------|
|                                            | average  | worst     | average   | worst     | average  | worst     |         |
| Hash table                                 | Θ(1)     | Θ(n)      | Θ(1)      | Θ(n)      | Θ(1)     | Θ(n)      | No      |
| Self-balancing<br>binary search tree       | Θ(log n) | Θ(log n)  | Θ(log n)  | Θ(log n)  | Θ(log n) | Θ(log n)  | Yes     |
| unbalanced<br>binary search tree           | Θ(log n) | Θ(n)      | Θ(log n)  | Θ(n)      | Θ(log n) | Θ(n)      | Yes     |
| Sequential container<br>of key-value pairs | Θ(n)     | Θ(n)      | Θ(1)      | Θ(1)      | Θ(n)     | Θ(n)      | No      |
| HDR Map                                    | Θ(1)     | Θ(1)      | Θ(1)      | Θ(1)      | Θ(1)     | Θ(1)      | Yes     |

Each map operation has constant time for HDR Map. 

### Magic of HDR Map

#### How maps are working

This algorithm was designed for arrays, I will use a array like example. Ok! We have a
array, the index of it is a unsigned byte. Its maximum length is `256`. Now we can define
it as a fixed size array `int[256]` or a dynamical array.

A dynamical array has an undefined size and by adding elements to it, the memory will be
reallocated. We can use segmentation to exclude reallocation and allocate new block just on
time. The question is: How to segmentalize correctly?

My answer is 16 blocks by 16 items. Because? The index is a byte, we drop it to 2 half and
get indices of element in 2D matrix (16 by 16). So `array[i]` will be equal to
`array.data[i >> 4][i & 0x0F]`.

This is not a limit, we can create 4D, 8D matrices. 4D matrix will be a array with a `uint16_t`
index. 8D matrix for an array with `uint32_t` index.

In that type of array any item can be absent or present, we named this data structure
`HDR Map`.

HDR Map has some modifications:
* Micro - blocks of 4 items and minimal memory usage.
* Common - blocks of 16 items and optimal memory usage.
* Fast - blocks of 256 items and extended memory usage.
* SuperFast - blocks of 65536 items and specialized for BigData.

Key an be one of the next types: `uint8_t`, `int8_t`, `uint16_t`, `int16_t`, `uint32_t`,
`int32_t`, `uint64_t`, `int64_t` and `__int128`.

#### Naming

The names of classes are composed in the next way:

`[U]Int<bits>[modification][ThreadSafe]Map`

Understanding:
* `U` is present for unsigned types.
* `Int` is always present.
* `bits` is the number of bits: `8`, `16`, `32`, `64` or `128`.
* `modification` is a modification: `Micro`, Common (skipped), `Fast`, `SuperFast`.
* `ThreadSafe` is present in thread-safe variations.
* `Map` is always present.

Table of available modifications for each integer type:

|     | Micro | Common | Fast | SuperFast |
|-----|-------|--------|------|-----------|
| 8   | ☑     |        |      |           |
| 16  | ☑     | ☑      |      |           |
| 32  | ☑     | ☑      | ☑    |           |
| 64  |       | ☑      | ☑    | ☑         |
| 128 |       |        | ☑    | ☑         |

## High Dynamical Range Array

High Dynamical Range Array (`hdr::Array`) is a data structure, which is similar to typical 
arrays, but has some advantage of hash tables.

### One more data structure? Why?

Let's see the comparative table:

| Criteria                      | Linked<br>list | Dynamic<br>array | Balanced<br>tree | Random<br>access list | Hashed<br>array tree | HDR Array |
| ------------------------------|----------------|------------------|------------------|-----------------------|----------------------|-----------|
| Indexing                      | Θ(n)           | Θ(1)             | Θ(log n)         | Θ(log n)              | Θ(1)                 | Θ(1)      |
| Insert/delete<br>at beginning | Θ(1)           | Θ(n)             | Θ(log n)         | Θ(1)                  | Θ(n)                 | Θ(1)      |
| Insert/delete<br>at end       | Θ(n)           | Θ(1)             | Θ(log n)         | Θ(log n)              | Θ(1)                 | Θ(1)      |
| Insert/delete<br>in middle    | search + Θ(1)  | Θ(n)             | Θ(log n)         | Θ(log n)              | Θ(n)                 | Θ(n/2)    |
| Wasted space<br>(average)     | Θ(n)           | Θ(n)             | Θ(n)             | Θ(n)                  | Θ(sqrt n)            | Θ(1)      |

Not released yet.
