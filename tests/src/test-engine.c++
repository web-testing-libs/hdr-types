#include "test-engine.h++"

#include "iostream"

void TestEngine::addTest(const std::function<void()> & test) {
    tests.push_back(test);
}

void TestEngine::testNow() {
    unknown = int(tests.size());

    for (auto & test : tests) {
        std::cout << "Testing... [" << successful << '/' << failed << '/'
                  << unknown << ']' << '\r' << std::flush;

        try {
            test();
            successful++;
        }
        catch (const std::exception & e) {
            std::cout << e.what() << ' ' << std::endl;
            failed++;
        }
        catch (...) {
            failed++;
        }
        unknown--;
    }

    std::cout << "Complete... [sucessful " << successful << " / failed "
              << failed << " / unknown " << unknown << ']' << '\n'
              << std::flush;
}
