#include "tests.h++"

#include "hdr-types/Map"
#include <iostream>



int main() {
    TestEngine te;

    using namespace icL::hdr;

    commonTest(Int8MicroMap);
    commonTest(Int8MicroThreadSafeMap);
    commonTest(UInt8MicroMap);
    commonTest(UInt8MicroThreadSafeMap);

    commonTest(Int16MicroMap);
    commonTest(Int16MicroThreadSafeMap);
    commonTest(UInt16MicroMap);
    commonTest(UInt16MicroThreadSafeMap);

    commonTest(Int16Map);
    commonTest(Int16ThreadSafeMap);
    commonTest(UInt16Map);
    commonTest(UInt16ThreadSafeMap);

    commonTest(Int32MicroMap);
    commonTest(Int32MicroThreadSafeMap);
    commonTest(UInt32MicroMap);
    commonTest(UInt32MicroThreadSafeMap);

    commonTest(Int32Map);
    commonTest(Int32ThreadSafeMap);
    commonTest(UInt32Map);
    commonTest(UInt32ThreadSafeMap);

    commonTest(Int32FastMap);
    commonTest(Int32FastThreadSafeMap);
    commonTest(UInt32FastMap);
    commonTest(UInt32FastThreadSafeMap);

    commonTest(Int64Map);
    commonTest(Int64ThreadSafeMap);
    commonTest(UInt64Map);
    commonTest(UInt64ThreadSafeMap);

    commonTest(Int64FastMap);
    commonTest(Int64FastThreadSafeMap);
    commonTest(UInt64FastMap);
    commonTest(UInt64FastThreadSafeMap);

    commonTest(Int64SuperFastMap);
    commonTest(Int64SuperFastThreadSafeMap);
    commonTest(UInt64SuperFastMap);
    commonTest(UInt64SuperFastThreadSafeMap);

    commonTest(Int128FastMap);
    commonTest(Int128FastThreadSafeMap);
    commonTest(UInt128FastMap);
    commonTest(UInt128FastThreadSafeMap);

    commonTest(Int128SuperFastMap);
    commonTest(Int128SuperFastThreadSafeMap);
    commonTest(UInt128SuperFastMap);
    commonTest(UInt128SuperFastThreadSafeMap);

    te.testNow();
    return 0;
}
