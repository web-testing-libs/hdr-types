#pragma once

#include <functional>
#include <iostream>
#include <list>



class TestEngine
{
    std::list<std::function<void()>> tests;

    int successful = 0;
    int failed     = 0;
    int unknown    = 0;

public:
    void addTest(const std::function<void()> & test);
    void testNow();
};

#include <cxxabi.h>
template <typename type>
void print(const type & addr) {
    char * name =
      abi::__cxa_demangle(typeid(addr).name(), nullptr, nullptr, nullptr);
    std::cout << "Test failed for " << name << '\n';
    delete[] name;
}

#define EQUALS(map, x, y, str)                                               \
    if ((x) != (y)) {                                                        \
        print(map);                                                          \
        std::cout << "Condition: Equal\nExpected: " << int(y)                \
                  << "\nGiven: " << int(x) << "\nMessage: " << (str) << '\n' \
                  << std::endl;                                              \
        throw(str);                                                          \
    }

#define NEQUALS(map, x, y, str)                                              \
    if ((x) == (y)) {                                                        \
        print(map);                                                          \
        std::cout << "Condition: Not equal\nUnexpected: " << int(y)          \
                  << "\nGiven: " << int(x) << "\nMessage: " << (str) << '\n' \
                  << std::endl;                                              \
        throw(str);                                                          \
    }
