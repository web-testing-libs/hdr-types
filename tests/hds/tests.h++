#include "test-engine.h++"

#define commonTest(L)                                                          \
    {                                                                          \
        static L<int> map;                                                     \
                                                                               \
        te.addTest(                                                            \
          [&]() { EQUALS(map, map.empty(), true, "Initially not empty") });    \
                                                                               \
        te.addTest(                                                            \
          [&]() { EQUALS(map, map.size(), 0, "Initial size is not 0") });      \
                                                                               \
        te.addTest(                                                            \
          [&]() { NEQUALS(map, map.maxSize(), 0, "Max size is not inited") }); \
                                                                               \
        te.addTest([&]() {                                                     \
            auto size = map.size();                                            \
            map.insert(0, 23);                                                 \
            EQUALS(map, map.size(), size + 1, "Size not updated on insert")    \
        });                                                                    \
                                                                               \
        te.addTest([&]() {                                                     \
            auto size = map.size();                                            \
            map.insert(100, 101);                                              \
            map.insert(125, 0);                                                \
            EQUALS(map, map.size(), size + 2, "Size not updated on insert")    \
        });                                                                    \
                                                                               \
        te.addTest([&]() {                                                     \
            auto size = map.size();                                            \
            map.erase(125);                                                    \
            EQUALS(map, map.size(), size - 1, "Size not updated on erase")     \
        });                                                                    \
                                                                               \
        te.addTest(                                                            \
          [&]() { EQUALS(map, map.at(100), 101, "Wrong value on read") });     \
                                                                               \
        te.addTest(                                                            \
          [&]() { EQUALS(map, map.at(0), 23, "Wrong value on read") });        \
                                                                               \
        te.addTest([&]() {                                                     \
            auto size = map.size();                                            \
            map.erase(10);                                                     \
            EQUALS(map, map.size(), size, "Size updated on wrong erase")       \
        });                                                                    \
    }                                                                          \
    te
